#include <stdio.h>
#include <stdlib.h>

int main (){

int x;

printf("Introduce un número: ");
scanf("%i", &x);

if(x > 1 && x < 10)

printf("El condicional cumple que es mayor que 1 Y que es menor que 10. \n");

else
if(x == 1 || x == 10)

printf("El condicional cumple que es igual que 1 Ó igual que 10.\n");

else
if(x != 3)

printf("El condicional cumple que es distinto de 3.\n");

return EXIT_SUCCESS;
}
