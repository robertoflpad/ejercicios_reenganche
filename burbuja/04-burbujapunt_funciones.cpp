#include <stdio.h>
#include <stdlib.h>

#define MAX 5

void fPrintar(int *array){

    for(int i=0; i<MAX; i++)
        printf("%i ", array[i]);
    printf("\n\n");


}

void fBubble(int *array){

    int aux;

    for (int i = 0; i < MAX; i++)
        for (int j = 0; j < MAX ; j++)
            if (*(array + i) > *(array + j)) {
                aux = *(array + i);
                *(array + i) = *(array + j);
                *(array + j) = aux;

            }

}

int main (int argc, char* argv[]){

    int array[MAX] = {3,6,44,-1,4};
    fBubble(array);


    fPrintar(array);
    return EXIT_SUCCESS;

}
