#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double f(double *pol, int grado, double x){
    double valor = 0;
    for (int i = 0; i <= grado; i++)
        valor  += ( *(pol + i) * pow(x,i));

    return valor;
}

int main (int argc, char *argv[]) {

    double x = 2;
    double *pol = NULL;
    double resultado = 0;
    int grado= 0;

    printf ("Introduce el grado de tu polinomio: ");
    scanf ("%i", &grado);

    printf ("Introduce %i valores del polinomio: ", grado+1);
    for(int prg = 0; prg <= grado; prg++){
        pol = (double *) realloc (pol, sizeof(double) * (prg + 1));
        printf("Número[%i] = ", prg+1);
        scanf ("%lf", pol + prg);
    }

        printf ("El resultado del polinomio sabiendo que x vale %.2lf es %.2lf.\n", x , f(pol, grado, x));

    free (pol);


    return EXIT_SUCCESS;
}
