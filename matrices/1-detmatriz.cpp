#include <stdio.h>
#include <stdlib.h>
#define MAX 3


int main (int argc, char* argv[]){

int parar = 1;
double a[MAX][MAX];
double 	num = 1,
	num1 = 1,
	num2 = 1,
	num3 = 1,
	num4 = 1,
	num5 = 1,
	res;

printf("CALCULAR EL DETERMINANTE DE UNA MATRIZ \n\n");

printf("Esto es una matriz de ejemplo:\n");
printf("a--> | 1 2 1 |\nb-->| 1 3 4 |\nc-->|-1 0 2 |\n\n");

printf("Se resolvería de la siguiente forma:\n");
printf("(a1*b2*c3)+(b1*c2*a3)+(c1*a2*b3)-(b1*a2*c3)+(a1*c2*b3)+(c1*b2*a3)");

printf("Dime la primera fila de la matriz:\n");

while(parar == 1) {

for (int i = 0; i < MAX; i++){
	for(int j = 0; j < MAX; j++){
	printf("\na[%i][%i] --> ", i, j);
		scanf("%lf", &a[i][j]);
			if((i==0 && j==0) || (i==1 && j==1) || (i==2 && j==2))
				num *= a[i][j];
			if((i==1 && j==0) || (i==2 && j==1) || (i==0 && j==2))
				num1 *= a[i][j];
			if((i==2 && j==0) || (i==0 && j==1) || (i==1 && j==2))
				num2 *= a[i][j];
			if((i==1 && j==0) || (i==0 && j==1) || (i==2 && j==2))
				num3 *= a[i][j];
			if((i==0 && j==0) || (i==2 && j==1) || (i==1 && j==2))
				num4 *= a[i][j];
			if((i==2 && j==0) || (i==1 && j==1) || (i==0 && j==2))
				num5 *= a[i][j];
			res = num+num1+num2-(num3+num4+num5);
		}
	}

printf("\n");
printf("El resultado es: %.2lf \n", res);

printf("¿Desea calcular otro determinante? (SI==>1 || NO==>0): ");
scanf("%i", &parar);
}

printf("\nGracias por utilizar esta aplicación.\n");

return EXIT_SUCCESS;

}
