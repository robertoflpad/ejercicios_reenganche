#include <stdio.h>
#include <stdlib.h>


int Frecurs(int n){
    int fact;
    if (n==0){
        return 1;
    }else{
        fact = n*Frecurs(n-1);

        return(fact);
    }
}

int main (int argc, char* argv[]){

    int n;

    printf("||%i||", Frecurs(7));

    printf("\n");

    return EXIT_SUCCESS;

}
