#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define MAX_DET 3


void fTitle(){
    system("clear");
    system("toilet -fpagga --gay Punteros a funciones");
    printf("\n\n\n\n");
}

double fEcuacion_segundo (double a, double b, double c) {

    double raiz = (pow(b, 2.))-(4*a*c),
           res_uno = (((b*-1) + sqrt(raiz))/(2*a));
    if(raiz<=0){
        return 0;
    }else
        return res_uno;
}

double fEcuacionNeg_segundo (double a, double b, double c) {

    double raiz = (pow(b, 2.))-(4*a*c),
           res_dos = (((b*-1) - sqrt(raiz))/(2*a));
    if(raiz<=0){
        printf("(- %.lf +- \u221A %.lf)/%.lf \n\n", b, raiz, 2*a);
        return 0;
    }else
        return res_dos;
}

void fEcuacionSeg(){

    double x, y, z;
    double (*p_res[2]) (double, double, double) = {fEcuacion_segundo, fEcuacionNeg_segundo};

    printf("Pon valores a las interrogaciones: ?x²+?x-?=0.\n");

    printf("?x²=");
    scanf("%lf", &x);
    printf("\n");

    printf("?x=");
    scanf("%lf", &y);
    printf("\n");

    printf("?=");
    scanf("%lf", &z);
    printf("\n");

    p_res[0](x,y,z);
    p_res[1](x,y,z);

    if(p_res[0](x,y,z) == 0){
        printf("La que has liao pollo\n\n");
    }
    else{
        printf("Primer resultado: %.2lf\nSegundo resultado: %.2lf\n", p_res[0](x,y,z), p_res[1](x,y,z));
    }
}

int indMat(int x){
    return x % MAX_DET;
}

int indMat_dos(int y){
    if(y<0)
        return y + MAX_DET;
    return y;
}

void fDeterminante(){

    double det_mat[MAX_DET][MAX_DET];
    double diag[MAX_DET-1][MAX_DET]={1, 1, 1,
        1, 1, 1};
    double res_uno, res_dos;

    for (int i=0; i<MAX_DET; i++)
        for (int j=0; j<MAX_DET; j++){
            printf("Posicion[%i][%i] = ", i, j);
            scanf("%lf" , &det_mat[i][j]);
            printf("\n");
        }

    for(int filas=0; filas<MAX_DET; filas++)
        for(int col=0; col<MAX_DET; col++)
            diag[0][filas] *= det_mat[indMat(filas + col)][indMat(col)];

    for(int filas=MAX_DET-1, filamat=0; filas>=0; filas--, filamat++)
        for(int col=MAX_DET-1; col>=0; col--)
            diag[1][filamat] *= det_mat[indMat_dos(filas - col)][indMat_dos(col)];

    for(int res = 0; res<MAX_DET; res++){
        res_uno += diag[0][res];
        res_dos += diag[1][res];
    }

    printf("El resultado es: %.2lf\n\n", res_uno-res_dos);

}

bool fPrimo(int num, int cont){

    if(cont < 2)
        return true;
    if(num%cont == 0)
        return false;
    return fPrimo(num, cont-1);

}

int fFactorial(int num) {

    if(num < 1)
        return 1;

    return num*fFactorial(num-1);

}

void fFactorialPrimo() {

    int primo;
    bool c_primo;

    printf("Mete un número, si es primo, te saco el factorial: ");
    scanf("%i", &primo);

    c_primo = fPrimo(primo, primo-1);

    if(c_primo == true)
        printf("No se factoriza, ya que este número es primo.\n\n");
    else{
        printf("NO ES PRIMO\n");

        printf("El factorial de este número es: %i \n", fFactorial(primo));

    }

}

void fMallocRealloc() {

    char *ml;
    ml = (char *) malloc(sizeof(char));

    printf("Introduce una página web: ");
    scanf("%s", ml);
    printf("\n\n");

    printf("La página web es: %s\n", ml);
    memset(ml, '-', 3);
    ml = (char *) realloc(ml, 4);

    printf("La página web es: %s\n\n", ml);

    free(ml);
}

void fMenu() {

    int op=-1;

    do{

        printf("Elige una operación matemática por defecto: \n\n");
        printf("0. Salir del programa.\n1. Ecuación de 2º grado.\n2. Determinante de una matriz de 3x3.\n3. Factorial/Priomo de un número.\n4. Malloc/Realloc ejemplo.\n---------Opción----------\n");
        scanf("%i", &op);

        switch(op){
            case 1:

                fEcuacionSeg();

                break;

            case 2:

                fDeterminante();

                break;

            case 3:

                fFactorialPrimo();

                break;

            case 4:

                fMallocRealloc();

                break;
        }


    }while(op!=0);

}

int main (int argc, char* argv[]){

    fTitle();

    fMenu();

    return EXIT_SUCCESS;

}
