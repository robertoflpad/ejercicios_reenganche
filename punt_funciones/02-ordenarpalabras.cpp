#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void fTitle(){
    system("clear");
    system("toilet -fpagga --metal Ordenar Palabras");

}

const char *words[] = {
    "mesa",
    "marmota",
    "Romualdo",
    "microfono",
    "silla",
    "urbanizacion",
    "piscina",
    NULL
};

void fVer( const char *a[] ) {
    for(int i=0; words[i] != NULL; i++)
        printf("%s ", words[i]);
    printf("\n");
}



int main (int argc, char* argv[]){

  int cont;

    fTitle();

    fVer(words);

    for(int i = 0; words[i+1] != NULL; i++){
        cont = i;
        for(int j=i+1; words[j] != NULL; j++)
            if(strcasecmp(words[j], words[cont]) < 0)
              cont = j;

        if(cont > i){
            const char *auxiliar = words[i];
            words[i] = words[cont];
            words[cont] = auxiliar;
        }
    }

    fVer(words);

    return EXIT_SUCCESS;

}
