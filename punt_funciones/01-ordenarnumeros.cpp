#include <stdio.h>
#include <stdlib.h>

#define MAX 5

void fPrintar(const int **p_array){

    for(int i=0; i<MAX; i++)
        printf("%i ", **(p_array+i));
        printf("\n\n");


}

void fBubble(const int **p_array){

    const int *aux;

    for (int i = 0; i < MAX; i++)
        for (int j = 0; j < MAX ; j++)
            if (**(p_array + i) > **(p_array + j)) {
                aux = *(p_array + i);
                *(p_array + i) = *(p_array + j);
                *(p_array + j) = aux;

            }

}

int main (int argc, char* argv[]){

    const int array[MAX] = {3,6,44,-1,4};
    const int *p_array[MAX];


    for(int i=0; i<MAX; i++)
    p_array[i] = &array[i];
    fBubble(p_array);


    fPrintar(p_array);


    return EXIT_SUCCESS;

}
