#include <stdio.h>
#include <stdlib.h>

void f_Title(){
    system("clear");
    system("toilet -fpagga --metal PUNTEROS");
    printf("\n");
    system("cowsay Paso por referencia");
    printf("\n\n\n");
};

void f_Escaner(double *x){

    printf("Dime un número que quieras guardar: ");
    scanf("%lf", x);
};

void f_Imprimir(){
  double x;
  f_Escaner(&x);
  printf("El numero es: %.2lf \n\n", x);
};

int main (){

    f_Title();

    f_Imprimir();


    return EXIT_SUCCESS;
}
