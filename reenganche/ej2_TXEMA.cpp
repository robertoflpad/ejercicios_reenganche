#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]){

unsigned int x = 0;

for(int i = 2; i<100; i+=3)
	x += i;

	printf("La suma de los números (con bucle for) es: %u \n", x);
	
	x = 0;

int i = 2;

do{

x += i;

i += 3;

} while(i < 100);

printf("La suma de los números (con bucle do/while) es: %u \n", x);

x = 0;

i = 2;
while(i < 100){
	x += i;
	i += 3;
}

printf("La suma de los números (con bucle while) es: %u \n", x);


return EXIT_SUCCESS;

}
