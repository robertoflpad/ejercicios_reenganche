#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (int argc, char* argv[]){

char color;
char minusculas;

printf("Dime una letra: ");
scanf("%c", &color);

minusculas=tolower(color);

switch(minusculas){

	case 'r':
		printf("Has elegido el color red: %c \n", minusculas);
	break;

	case 'g':
		printf("Has elegido el color green: %c \n", minusculas);
	break;

	case 'b':
		printf("Has elegido el color blue: %c \n", minusculas);
	break;

	default:
		printf("No existe dicho color. \n");
	break;

}


return EXIT_SUCCESS;

}
