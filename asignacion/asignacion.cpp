#include <stdio.h>
#include <math.h>

int main (){
	int x, y;
	x = 10;
	y = 2;

	//Operaciones básicas

	x = x + y;
	printf("El valor de x+y es igual %i\n", x);

	x = 10;
	x = x - y;
	printf("El valor de x-y es igual %i\n", x);

	x = 10;
	x = x / y;
	printf("El valor de x/y es igual %i\n", x);

	x = 10;
	x = x * y;
	printf("El valor de x*y es igual %i\n", x);

	x = 10;
	x = x % y;
	printf("El valor de x resto y es igual %i\n", x);

	x = 10;
	x = pow (y,x);
	printf("El valor de y elevado a x es igual a %i\n", x);

	x = sqrt (x);
	printf("El valor de la raiz de x es: %i\n", x);

	return 0;

}
