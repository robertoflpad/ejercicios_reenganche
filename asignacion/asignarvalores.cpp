//ASIGNACIÓN DE VALORES

#include <stdio.h>
#define PI 3.1416

int main (){
	float h;
	float r;
	float x;

	printf("|x|Calculo del volumen de un cilindro|x|");
	printf("\n\n\n");
	printf("Introduce el valor del radio: ");
	scanf("%f", &r);//Guardo el valor en la dirección de "r".
	printf("\nIntroduce el valor de la altura: ");
	scanf("%f", &h);
	printf("\nLa formula es A=PI*r*r*h.\n");
	x = PI*r*r*h;
	printf("El resultado es: %f\n", x);


	return 0;
}
