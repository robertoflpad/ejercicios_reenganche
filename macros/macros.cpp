//MACROS

#include <stdio.h>
#define PI 3.1416 //Hemos declarado la macro "PI" con valor: 3,1416

int main (){
	int x;

	printf("|x|Calcula el volumen de un cilindro|x|");

	printf("\n\n\n");

	printf("La fórmula es: PI*r^2*h\n");
	printf("La altura(h) es 2.\n");
	printf("El radio(r) es 5. \n");
	printf("PI es la macro ya definida, que es %.2f. \n", PI);
	printf("El resultado es: %.3f.\n", PI*2*2*5);

return 0;
}
