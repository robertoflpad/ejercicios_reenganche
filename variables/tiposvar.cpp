//TIPOS DE VARIABLE

#include <stdio.h>

int main (){
	int x; //Enteros: -2^16 hasta 2^16 bits
	float y; //Flotantes: -2^32 hasta 2^32 bits
	double y2; //Flotantes: -2^64 hasta 2^64 bits
	char z; //Caracter: Letras y palabras

	x=5; //Inicialización de la variable int (o darle valor)
	y=10.4;
	y2=9.7;
	z='a';

	printf("int: %i \nfloat: %f \ndouble: %f \nchar: %c \n", x, y, y2, z);

	return 0;

}
