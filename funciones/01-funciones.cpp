#include <stdio.h>
#include <stdlib.h>


void title(){

    printf("---------------------------\n");
    printf("|        TITULO           |\n");
    printf("---------------------------\n");
    printf("\n\n\n");

}

double escanear(){

    double x;
    printf("Introduzca el numero: ");
    scanf("%lf", &x);
    return x;
}

void imprimir (double y){

    printf("\n El numero es: %lf \n", y);
}

int main (int argc, char* argv[]){

    double ejemplo;
    title();
    ejemplo = escanear();

    imprimir(ejemplo);

    return EXIT_SUCCESS;

}
