#include <stdio.h>
#include <stdlib.h>


void title(){

    printf("---------------------------\n");
    printf("|        TITULO           |\n");
    printf("---------------------------\n");
    printf("\n\n\n");

}

void escan_imp(double *x){

    printf("Introduzca el numero: ");
    scanf("%lf", x);
    printf("\n El numero es: %.1lf \n", *x);

}

int main (int argc, char* argv[]){

    double num;
    title();
    escan_imp(&num);

    return EXIT_SUCCESS;

}
